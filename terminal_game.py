import curses
from braille_engine import BrailleEngine
from time import sleep

width = 250
height = 250

def main():
    stdscr = curses.initscr()
    br = BrailleEngine(stdscr)
    
    #screen = init_screen(width, height) 
    screen = br.generate_screen(250,250)
    br.draw_screen(screen)
    #while True: 
    #    screen[125][125] = 1
    #    br.draw_screen(screen)
    #
    #    sleep(0.5)

    #    screen[125][125] = 0
    #    br.draw_screen(screen)

    #    sleep(0.5)

    stdscr.getch()
    curses.endwin()

def init_screen(width, height):
    #set terminal to size
    
    #create empty 2d array
    screen = []
    for i in range (0, height):    
        new = []                  
        for j in range (0, width):  
            new.append(0)     
        screen.append(new)

    return screen


if __name__ == "__main__":
    main()
