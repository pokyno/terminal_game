import sys
import curses

import locale
locale.setlocale(locale.LC_ALL, '')

###########################################################################################################################################################################################################################################
#THE CONVERSION BETWEEN X,Y TO A BRAILLE REPRESENTATION
######################################################################################################################################################################################################################################
#get the python version
py_ver_3 = sys.version_info[0] == 3
#The standard startpoint for brail unichars
brail_base = 10240

#the binary should be formatted as such
#                             ---
#87654321 for brail positions|1 4| 
#00000000                    |2 5|
#                            |3 6|
#                            |7 8|
#                             ---
#So the binary                ---
#01010101  will look like    |.  |
#                            |  .|
#                            |.  |
#                            |.  |
#                             ---
def binary_to_unichar(binary):
    #TODO ADD ERROR CHECKS
    brail = int(binary, 2)
    uni = brail_base + brail 
        
    if py_ver_3:
        return chr(uni)
    else:
        return unichr(uni)

framewidth = 2
frameheight = 4

#returns the unicode that belongs to the frame with the given x an y offset
#screen is [y][x]
def get_frame_unicode(xoffset, yoffset, screen):
    #TODO ADD ERROR CHECKS
    frame = ""
    frame += str( screen[yoffset * frameheight + (frameheight - 1)][xoffset * framewidth + (framewidth - 1)]) #pos 8 
    frame += str( screen[yoffset * frameheight + (frameheight - 1)][xoffset * framewidth + (framewidth - 2)]) #pos 7 
    frame += str( screen[yoffset * frameheight + (frameheight - 2)][xoffset * framewidth + (framewidth - 1)]) #pos 6 
    frame += str( screen[yoffset * frameheight + (frameheight - 3)][xoffset * framewidth + (framewidth - 1)]) #pos 5 
    frame += str( screen[yoffset * frameheight + (frameheight - 4)][xoffset * framewidth + (framewidth - 1)]) #pos 4 
    frame += str( screen[yoffset * frameheight + (frameheight - 2)][xoffset * framewidth + (framewidth - 2)]) #pos 3 
    frame += str( screen[yoffset * frameheight + (frameheight - 3)][xoffset * framewidth + (framewidth - 2)]) #pos 2 
    frame += str( screen[yoffset * frameheight + (frameheight - 4)][xoffset * framewidth + (framewidth - 2)]) #pos 1 
    
    return binary_to_unichar(frame)

def check_screen(screen):
    pass 
    #check y size

    #check x size

    #if the screen is not sized properly fix that


def parse_screen(screen):
    #TODO ADD ERROR CHECKS
    
    screen_x_frame_count = int(len(screen[0]) / framewidth)
    screen_y_frame_count = int(len(screen) / frameheight)
    
    parsed_screen = []
    for i in range(screen_y_frame_count):
        #x for the array
        x = []
        for j in range(screen_x_frame_count):
            x.append(get_frame_unicode(j, i, screen))   
        parsed_screen.append(x)

    return parsed_screen

def curses_draw_screen(screen,stdscr):
        #screen = check_screen()

        screen_x_frame_count = int(len(screen[0]) / framewidth)
        screen_y_frame_count = int(len(screen) / frameheight)

        try:
            for i in range(screen_y_frame_count):
                for j in range(screen_x_frame_count):
                    if py_ver_3:
                        stdscr.addstr(i,j, get_frame_unicode(j, i, screen)) 
                    else:
                        stdscr.addstr(i,j, get_frame_unicode(j, i, screen).encode("UTF-8")) 
        except:
            print("a problem has occured. Maby stdscr was not initialized")

import random
def generate_screen(width, height):
    screen = []
        
    for i in range(height):
        x = []
        for j in range(width):
            value = int(random.choice('10'))
            x.append(value)

        screen.append(x)

    return screen


def init():
    try:
        stdscr = curses.initscr()

        curses.noecho()
        curses.cbreak()
        stdscr.keypad(1)

        screen = generate_screen(250,250)
        curses_draw_screen(screen, stdscr)

        stdscr.getch()
        curses.endwin()
    except:
        curses_error()

def curses_error():
    print("an error occurred restoring terminal")
    curses.endwin()

init()
