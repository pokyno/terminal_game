import sys
import curses

import locale
locale.setlocale(locale.LC_ALL, '')

import random

####################################################################################################
#THE CONVERSION BETWEEN X,Y TO A BRAILLE REPRESENTATION
####################################################################################################
class BrailleEngine:

    #A flag to see if this is python ver 3
    py_ver_3 = sys.version_info[0] == 3

    #The standard startpoint for braill unichars
    braille_base = 10240

    framewidth = 2
    frameheight = 4

    def __init__(self, stdscr):
        try:
            self.stdscr = stdscr
            curses.noecho()
            curses.cbreak()
            self.stdscr.keypad(1)            
        except:
            curses_error()

    def curses_error(self):
        print("an error occurred restoring terminal")
        curses.endwin()
       

    #the binary should be formatted as such
    #                             ---
    #87654321 for brail positions|1 4| 
    #00000000                    |2 5|
    #                            |3 6|
    #                            |7 8|
    #                             ---
    #So the binary                ---
    #01010101  will look like    |.  |
    #                            |  .|
    #                            |.  |
    #                            |.  |
    #                             ---
    def binary_to_unichar(self, binary):
        #TODO ADD ERROR CHECKS
        brail = int(binary, 2)
        uni = self.braille_base + brail 
            
        if self.py_ver_3:
            return chr(uni)
        else:
            return unichr(uni)
     
        #returns the unicode that belongs to the frame with the given x an y offset
    #screen is [y][x]
    def get_frame_unicode(self, xoffset, yoffset, screen):
        #TODO ADD ERROR CHECKS
        frame = ""
        frame += str( screen[yoffset * self.frameheight + (self.frameheight - 1)][xoffset * self.framewidth + (self.framewidth - 1)])#pos 8
        frame += str( screen[yoffset * self.frameheight + (self.frameheight - 1)][xoffset * self.framewidth + (self.framewidth - 2)])#pos 7
        frame += str( screen[yoffset * self.frameheight + (self.frameheight - 2)][xoffset * self.framewidth + (self.framewidth - 1)])#pos 6
        frame += str( screen[yoffset * self.frameheight + (self.frameheight - 3)][xoffset * self.framewidth + (self.framewidth - 1)])#pos 5
        frame += str( screen[yoffset * self.frameheight + (self.frameheight - 4)][xoffset * self.framewidth + (self.framewidth - 1)])#pos 4
        frame += str( screen[yoffset * self.frameheight + (self.frameheight - 2)][xoffset * self.framewidth + (self.framewidth - 2)])#pos 3
        frame += str( screen[yoffset * self.frameheight + (self.frameheight - 3)][xoffset * self.framewidth + (self.framewidth - 2)])#pos 2
        frame += str( screen[yoffset * self.frameheight + (self.frameheight - 4)][xoffset * self.framewidth + (self.framewidth - 2)])#pos 1 
        
        return self.binary_to_unichar(frame)
    
    def check_screen(self, screen):
        pass 
        #check y size
    
        #check x size
    
        #if the screen is not sized properly fix that
    
    
    def parse_screen(self, screen):
        #TODO ADD ERROR CHECKS
        
        screen_x_frame_count = int(len(screen[0]) / self.framewidth)
        screen_y_frame_count = int(len(screen) / self.frameheight)
        
        parsed_screen = []
        for i in range(screen_y_frame_count):
            #x for the array
            x = []
            for j in range(screen_x_frame_count):
                x.append(self.get_frame_unicode(j, i, screen))   
            parsed_screen.append(x)
    
        return parsed_screen
    
    def draw_screen(self, screen):
            #screen = check_screen()
            screen_x_frame_count = int(len(screen[0]) / self.framewidth)
            screen_y_frame_count = int(len(screen) / self.frameheight)
    
            try:
                for i in range(screen_y_frame_count):
                    for j in range(screen_x_frame_count):
                        if self.py_ver_3:
                            self.stdscr.addstr(i,j, self.get_frame_unicode(j, i, screen)) 
                        else:
                            self.stdscr.addstr(i,j, self.get_frame_unicode(j, i, screen).encode("UTF-8")) 
            except:
                print("a problem has occured. Maby stdscr was not initialized")
    
    def generate_screen(self, width, height):
        screen = []
            
        for i in range(height):
            x = []
            for j in range(width):
                value = int(random.choice('10'))
                x.append(value)
    
            screen.append(x)
    
        return screen

#for module testing
if __name__ == "__main__":
    std_scr = curses.initscr() 
    braille_sys = BrailleEngine(std_scr)
    screen = braille_sys.generate_screen(250,250)
    braille_sys.curses_draw_screen(screen)
    
    braille_sys.stdscr.getch()
    curses.endwin()
